package com.ukefu.webim.web.handler.apps;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.ukefu.core.UKDataContext;
import com.ukefu.util.Menu;
import com.ukefu.webim.service.repository.OnlineUserRepository;
import com.ukefu.webim.service.repository.UserRepository;
import com.ukefu.webim.web.handler.Handler;

@Controller
public class AppsController
  extends Handler
{
  @Autowired
  private UserRepository userRepository;
  @Autowired
  private OnlineUserRepository onlineUserRes;
  
  @RequestMapping({"/apps/content"})
  @Menu(type="apps", subtype="content")
  public ModelAndView content(HttpServletRequest request)
  {
    ModelAndView view = request(super.createAppsTempletResponse("/apps/content"));
    view.addObject("onlineUserList", this.onlineUserRes.findByOrgiAndStatus(super.getOrgi(request), UKDataContext.OnlineUserOperatorStatus.ONLINE.toString(), new PageRequest(super.getP(request), super.getPs(request), Sort.Direction.DESC, new String[] { "createtime" })));
    return view;
  }
}
