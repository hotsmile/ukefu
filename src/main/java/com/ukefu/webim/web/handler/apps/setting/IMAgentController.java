package com.ukefu.webim.web.handler.apps.setting;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.ukefu.util.Menu;
import com.ukefu.webim.service.repository.ConsultInviteRepository;
import com.ukefu.webim.web.handler.Handler;

@Controller
@RequestMapping("/setting/agent")
public class IMAgentController extends Handler{
	
	@Autowired
	private ConsultInviteRepository invite;
	
	@Value("${web.upload-path}")
    private String path;

    @RequestMapping("/index")
    @Menu(type = "setting" , subtype = "index" , admin= true)
    public ModelAndView index(ModelMap map , HttpServletRequest request) {
        return request(super.createAppsTempletResponse("/apps/setting/agent/index"));
    }
    
    @RequestMapping("/blacklist")
    @Menu(type = "setting" , subtype = "blacklist" , admin= true)
    public ModelAndView blacklist(ModelMap map , HttpServletRequest request) {
        return request(super.createAppsTempletResponse("/apps/setting/agent/blacklist"));
    }
    
    @RequestMapping("/tag")
    @Menu(type = "setting" , subtype = "tag" , admin= true)
    public ModelAndView tag(ModelMap map , HttpServletRequest request) {
        return request(super.createAppsTempletResponse("/apps/setting/agent/tag"));
    }
    
    @RequestMapping("/acd")
    @Menu(type = "setting" , subtype = "acd" , admin= true)
    public ModelAndView acd(ModelMap map , HttpServletRequest request) {
        return request(super.createAppsTempletResponse("/apps/setting/agent/acd"));
    }
    
    @RequestMapping("/kuaijie")
    @Menu(type = "setting" , subtype = "kuaijie" , admin= true)
    public ModelAndView kuaijie(ModelMap map , HttpServletRequest request) {
        return request(super.createAppsTempletResponse("/apps/setting/agent/kuaijie"));
    }
}