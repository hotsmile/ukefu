package com.ukefu.webim.service.repository;

import com.ukefu.webim.web.model.OnlineUser;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public abstract interface OnlineUserRepository
  extends JpaRepository<OnlineUser, String>
{
  public abstract OnlineUser findBySessionid(String paramString);
  
  public abstract Page<OnlineUser> findByOrgiAndStatus(String paramString1, String paramString2, Pageable paramPageable);
}
