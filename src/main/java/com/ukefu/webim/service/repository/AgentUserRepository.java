package com.ukefu.webim.service.repository;

import com.ukefu.webim.web.model.AgentUser;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public abstract interface AgentUserRepository
  extends JpaRepository<AgentUser, String>
{
  public abstract AgentUser findById(String paramString);
  
  public abstract AgentUser findByUserid(String paramString);
  
  public abstract List<AgentUser> findByAgentno(String paramString);
}
