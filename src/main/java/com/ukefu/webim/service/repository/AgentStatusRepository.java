package com.ukefu.webim.service.repository;

import com.ukefu.webim.web.model.AgentStatus;
import org.springframework.data.jpa.repository.JpaRepository;

public abstract interface AgentStatusRepository
  extends JpaRepository<AgentStatus, String>
{
  public abstract AgentStatus findById(String paramString);
}
